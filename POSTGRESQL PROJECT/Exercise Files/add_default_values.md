# STEPS
1. Expand the table
2. Expand the columns
3. Right click on the column you want to add a default value to.
4. Click properties
5. [General] -> Leave column_name as is.
6. [Constraints] -> Set the default value.
7. [SQL] -> ALTER TABLE IF EXISTS public.employees
                ALTER COLUMN department_id SET DEFAULT 800;


                