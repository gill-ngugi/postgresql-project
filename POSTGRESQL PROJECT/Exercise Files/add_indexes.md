# STEPS
<!-- Indexes help PostgreSQL locate records faster. -->
Right click on a table -> Create -> Index

Under [General] tab -> name of index -> employees_employees_id_idx
--------
Format
--------
employees_employees_id_idx
    employees -> table name
    employees_id -> column name
    idx -> just shows its an index

Under [Definition] Tab
Access method -> btree
Everything else -> Leave as defaults are
Click on the plus icon -> key in the column you wish to add an index to.
    Sort order -> ASC
    NULLs -> Last

Under [SQL] Tab
CREATE INDEX employees_employees_id_idx
    ON public.employees USING btree
    (employee_id ASC NULLS LAST)
;

Then click SAVE.

#  TO VERIFY CREATION OF INDEX
Expand the table
Expand the indexes -> You should view the created index.
