# STEPS
------------
Back-Up DB
------------
1. Right click database name -> Backup
<!-- /.../POSTGRESQL/Exercise Files/collections/newRealityDbBackup -->
   [General]
2. Filename -> This is where the backup file will be stored in your computer
3. Format -> Choose 'sql' or 'backup' options
4. Then click create
5. Format -> tar option -> creates an archive file
   
   [Data/Objects] -> This is where you select exactly what the backup contains.
6.  Under sections.
    Select pre-data -> This includes the tables, column structures and schemas of the DB
          data -> This includes data from the tables.
          post-data -> This includes things like views, indexes and constraints.

7. Click Backup

8. Verify by going to the file explorer and confirming the backup was actually saved. 

------------
Restore DB
------------
1. Expand PostgreSQL 14
2. Expand Databases -> Right click Databases -> Create -> Database
3. Right click on the new database name -> Click Restore
   [General]
4. Format -> Custom or tar
5. Filename -> Select location you saved the backup DB.
   [Data/Objects]
    Select pre-data -> This includes the tables, column structures and schemas of the DB
        data -> This includes data from the tables.
        post-data -> This includes things like views, indexes and constraints.
6. Click Restore



