# STEPS
<!-- This constraint will ensure that all new employees to be created are hired after 2020-01-01 -->
<!-- If an employee is created and the hire date is set to before 2020-01-01, the data won't be saved.  -->
1. Expand table name

2. Right click constraints -> create -> Check

3. [General] -> Name -> employees_hire_date_check
                employees -> Table name
                hire_date -> Column which we're adding the constraint to
                check -> Is coz this is a check constraint.

<!-- Another example of step 4 is -> [market='marikiti' OR market='soko ya mawe'] -->
4. [Definition] -> Check -> hire_date > '2020-01-01'
                -> No inherit
                    <!-- Some tables can inherit columns from other tables in a hierarchy -->
                    if yes: This will define that this constraint should also apply to child tables
                    if no: This defines that this constraint should not be applied to child tables
                -> Don't validate
                    if yes: It will only be applied to new data
                    if no: It will be applied to also the existing data

5. [SQL] -> ALTER TABLE IF EXISTS public.employees
                ADD CHECK (hire_date > '2020-01-01')
                NOT VALID;

6. Click SAVE