# QUERY THAT CREATES A VIEW AND JOINS TWO RELATED TABLES
<!-- Views act like tables but they dont duplicate data -->
CREATE VIEW manufacturing.product_details AS

SELECT products.product_id,
	products.name AS products_name,
	products.manufacturing_cost,
	categories.name AS category_name,
	categories.market

FROM manufacturing.products JOIN manufacturing.categories
	ON products.category_id = categories.category_id
;

# TO SEE THE CREATED VIEW
1. Expand the schema
2. Scroll down to views
3. Expand the views -> You should be able to see the created view.
4. Expand the columns to view the columns created


# TO RUN A QUERY TO GET THE DETAILS IN THE VIEW
SELECT * FROM manufacturing.product_details;

OR

SELECT * FROM manufacturing.product_details
WHERE category_name = 'wind_harvesters';


