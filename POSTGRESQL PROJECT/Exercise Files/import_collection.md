# STEPS
1. Create a table in a schema
2. Right click the table -> Choose import/export data
3. Then toggle the buttone to read IMPORT
4. Select the location of the file to be imported.
5. Select encoding as UTF8 -> header (yes) -> Delimiter (,)
