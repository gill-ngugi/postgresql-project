# Create a query that joins related tables together

# Query 1
SELECT products.product_id,
	products.name AS products_name,
	products.manufacturing_cost,
	categories.name AS category_name,
	categories.market

FROM manufacturing.products JOIN manufacturing.categories
	ON products.category_id = categories.category_id
	
WHERE market = 'industrial';

# Query 2
<!-- employees and departments are tables -->
<!-- fname, lname, department name and building are columns in those tables. -->
SELECT employees.first_name,
	employees.last_name,
	departments.department_name,
	departments.building

<!-- human_resources is the schema -->
<!-- Table Relationship is that the department_id columns in the employees and departments tables are similar -->
FROM human_resources.employees JOIN human_resources.departments
	ON employees.department_id = departments.department_id

WHERE building = 'South';