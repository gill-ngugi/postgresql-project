# STEPS
1. Launch pgadmin from applications.
2. Set master password.
3. Expand databases -> Then expand the specific db
4. Expand schemas -> public -> Tables 
5. Right click on the table name you want -> Click View/Edit data -> All rows.

# Open SQL Editor
1. Click tools at the menubar where there is pgadmin
2. Expand tools -> Click Query tool.

# Create A DB
1. Right click Postgresql.
2. Click create -> Database -> Enter Db name -> Click save.

# Create Schema
1. Expand databases -> Then expand the specific db
2. Right click on schemas -> create -> schema -> enter schema name -> save.

# Create A Table
1. Click Databases -> Click gillDb -> Expand schemas 
2. Right click on the schema you want -> Create -> Table -> Enter name -> save.

# Scripts
1. Right click on the table name
2. Click scripts -> INSERT SCRIPT-> Edit accordingly.




