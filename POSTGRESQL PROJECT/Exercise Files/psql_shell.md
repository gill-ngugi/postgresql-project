# NOTES
RDBMS - Relational Database Management System
SQL - Structured Query Language


# STEPS
1. https://www.postgresql.org/download/
   -> Click the link, "Download the installer"
2. Pword -> root4321
3. Port -> 5433
4. Now you have psql shell and pgadmin 4
5. Open psql -> enter the pword above

Commands
-----------
\l -> list of databases


6. SELECT version(); ... To check version of PSQL

7. SELECT now(); ... To check what time it is now.

8. CREATE DATABASE gillDb;

9. \l ... This displays a list of all the databases on the Postgres server.

10. \c gillDb ... To switch to another db such as gillDb.

11. CREATE TABLE countries (id int, name char(40));

12. INSERT INTO countries VALUES (1, 'UK'), (2, 'Canada'), (3, 'Germany');

13. SELECT * FROM countries;


# --------------------------------------
SELECT version();

SELECT now();

CREATE DATABASE favoritecolors;

\l

\c favoritecolors

CREATE DATABASE colors (ColorID int, ColorName char(20));

INSERT INTO colors VALUES (1, 'red'), (2, 'blue'), (3, 'green');

SELECT * FROM colors;

