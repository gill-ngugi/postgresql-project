# Super User
postgres -> The superuser account has the ability to create and do anything on the server.
         -> It has the most privileges out of any of the other user accounts.
         -> This includes the ability to create new roles and assign permissions.

# Create A New Role
1. Expand PostgreSQL 14
2. Expand Login/Group Roles -> Right click -> Create -> Login/Group Roles
3. When done setting up the role -> Click save.

# To Edit A Role
1. Right click on the role
2. Click properties
3. Edit accordingly

# Switch to start using the new role.
1. Run the command below in the Query Editor
2. SET ROLE role_name_created;
3. Postgres will start using the role above.

# Reset to superuser postgres
1. RESET ROLE;

# View tables from the database
SELECT * FROM manufacturing.products;
SELECT * FROM human_resources.employees;

# After creating the role hr_manager, impersonate the hr_manager
SET ROLE hr_manager;

# Switch permissions back to posgres super user
RESET ROLE;

# Give hr_manager permissions in database
GRANT USAGE ON SCHEMA human_resources TO hr_manager;
GRANT SELECT ON ALL TABLES IN SCHEMA human_resources TO hr_manager;
<!-- OR -->
GRANT INSERT ON ALL TABLES IN SCHEMA human_resources TO hr_manager;
<!-- OR -->
GRANT DELETE ON ALL TABLES IN SCHEMA human_resources TO hr_manager;
GRANT ALL ON ALL TABLES IN SCHEMA human_resources TO hr_manager;

# Remove the hr_manager role from Postgres Server
RESET ROLE;
REVOKE ALL ON ALL TABLES IN SCHEMA human_resources FROM hr_manager;
REVOKE USAGE ON SCHEMA human_resources FROM hr_manager;
DROP ROLE hr_manager;

